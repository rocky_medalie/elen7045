package za.org.jcse.elen7045.rocky.dodge.controllers;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import za.org.jcse.elen7045.rocky.dodge.game.GameEngine;

public class KeyboardInputController extends KeyAdapter {
	private int pressCount = 1;
	private GameEngine game;
	private int LEFT = StaticVariableController.LEFT_KEY;
	private int RIGHT = StaticVariableController.RIGHT_KEY;
	
	public KeyboardInputController(GameEngine game) {
		this.game = game;
	}

	//AWT notifies that a key has been pressed. 
	public void keyPressed(KeyEvent e) {
		// if we're waiting for an "any key" typed then we don't  want to do anything with just a "press"
		if (game.isWaitingToStart()) {
			return;
		}
		//notify the game which key was pressed so it can act accordingly
		if (e.getKeyCode() == LEFT) {
			game.setLeftArrowPressed(true);
		}
		if (e.getKeyCode() == RIGHT) {
			game.setRightArrowPressed(true);
		}
	} 

	public void keyReleased(KeyEvent e) {
		// if we're waiting for an "any key" typed then we don't want to do anything with just a "released"
		if (game.isWaitingToStart()) {
			return;
		}
		
		if (e.getKeyCode() == LEFT) {
			game.setLeftArrowPressed(false);
		}
		if (e.getKeyCode() == RIGHT) {
			game.setRightArrowPressed(false);
		}
	}

	public void keyTyped(KeyEvent e) {
	
		if (game.isWaitingToStart()) {
			if (pressCount == 1) {
				game.setWaitingToStart(false);
				game.startGame();
				pressCount = 0;
			} else {
				pressCount++;
			}
		}
		
		// if we hit escape, then quit the game
		if (e.getKeyChar() == 27) {
			System.exit(0);
		}
	}

}
