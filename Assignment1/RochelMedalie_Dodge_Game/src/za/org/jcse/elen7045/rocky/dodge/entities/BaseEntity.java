package za.org.jcse.elen7045.rocky.dodge.entities;

import java.awt.Graphics;
import java.awt.Rectangle;

import za.org.jcse.elen7045.rocky.dodge.controllers.SpriteController;
import za.org.jcse.elen7045.rocky.dodge.interfaces.BaseEntityInterface;

public abstract class BaseEntity implements BaseEntityInterface {

	protected double x; //x location
	protected double y; //y location
	protected Sprite sprite; // The sprite that represents this entity
	protected double dx; // The horizontal entity speed (pixels/sec) 
	protected double dy; //The vertical entity speed (pixels/sec)
	private Rectangle thisEntity = new Rectangle();  //rectangle that represents the current object during collision
	private Rectangle otherEntity = new Rectangle(); //rectangle that represents the other object that it collided with
	protected double moveSpeed;
	
	//create an entity using a prite image, and the x and co-ordinates location
	//get the image from the cached map
	public BaseEntity(String imageRef,int x,int y) {
		this.sprite = SpriteController.get().getSprite(imageRef);
		this.x = x;
		this.y = y;
	}
	
	//Move the entity based on a certain amount of time having passed in milliseconds
	public void move(long delta) {
		// update the location of the entity based on move speeds
		x += (delta * dx) / 1000;
		y += (delta * dy) / 1000;
	}
	
	//speeds are in pixels/sec
	public void setHorizontalSpeed(double dx) {
		this.dx = dx;
	}

	public void setVerticalSpeed(double dy) {
		this.dy = dy;
	}
	
	public double getHorizontalSpeed() {
		return dx;
	}

	public double getVerticalSpeed() {
		return dy;
	}
	
	//draw the entity to the graphics context
	public void draw(Graphics g) {
		sprite.draw(g,(int) x,(int) y);
	}
	
	//x location
	public int getX() {
		return (int) x;
	}

	//y location
	public int getY() {
		return (int) y;
	}

	//check if the entity collided with another one
	public boolean collidesWith(BaseEntity other) {
		thisEntity.setBounds((int) x,(int) y,sprite.getWidth(),sprite.getHeight());
		otherEntity.setBounds((int) other.x,(int) other.y,other.sprite.getWidth(),other.sprite.getHeight());

		return thisEntity.intersects(otherEntity);
	}
	
	//delegate the implementation of the way the collision is handled to the specific entity class
	public abstract void handleCollision(BaseEntity other);
	
	//this is the max speed the entity can move at
	public double getMoveSpeed() {
		return moveSpeed;
	}
	
}