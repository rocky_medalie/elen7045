package za.org.jcse.elen7045.rocky.dodge.interfaces;

import java.awt.Graphics;

import za.org.jcse.elen7045.rocky.dodge.entities.BaseEntity;

/*
 * defines the interface for the basic methods that the game entities must implement 
 */

public interface BaseEntityInterface {
	
	public void move(long delta);
	public void setHorizontalSpeed(double dx);
	public void setVerticalSpeed(double dy);
	public void draw(Graphics g); 
	public boolean collidesWith(BaseEntity other);
	public abstract void handleCollision(BaseEntity other);
}
