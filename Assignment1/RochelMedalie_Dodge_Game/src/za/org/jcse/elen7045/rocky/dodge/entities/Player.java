package za.org.jcse.elen7045.rocky.dodge.entities;

import za.org.jcse.elen7045.rocky.dodge.controllers.StaticVariableController;
import za.org.jcse.elen7045.rocky.dodge.game.GameEngine;

/*
 * this class represents the player of the game.
 * it can only move horizontally, represented by the dx co-ordinates  
 * we don't implement any collision handling logic here, it done in the raindrop class
 */

public class Player extends BaseEntity {

	private double moveSpeed = StaticVariableController.PLAYER_MOVE_SPEED;
	private GameEngine game;
	
	public Player(GameEngine game,String ref,int x,int y) {
		super(ref,x,y);
		this.game = game;
	}
	
	public void move(long delta) {
		// ensure the player isn't moved off the screen
		// if we're moving left and have reached the left hand side of the screen, don't move
		if ((dx < 0) && (x < StaticVariableController.LEFT_BORDER)) {
			return;
		}
		// if we're moving right and have reached the right hand side of the screen, don't move
		if ((dx > 0) && (x > StaticVariableController.RIGHT_BORDER)) {
			return;
		}
		//otherwise continue with a normal move
		super.move(delta);  //delta is the time that has elapsed since last move in ms
	}

	public void handleCollision(BaseEntity other) {
		return;
	}

	public double getMoveSpeed() {
		return moveSpeed;
	}
	
}
