package za.org.jcse.elen7045.rocky.dodge.controllers;

import java.util.ArrayList;
import java.util.Random;

import za.org.jcse.elen7045.rocky.dodge.entities.BaseEntity;
import za.org.jcse.elen7045.rocky.dodge.entities.GameFloor;
import za.org.jcse.elen7045.rocky.dodge.entities.Player;
import za.org.jcse.elen7045.rocky.dodge.entities.Raindrop;
import za.org.jcse.elen7045.rocky.dodge.game.GameEngine;

/*
 * this class is used initialise all the entities in the game.
 * it creates the sprite with a graphic, and the x and y co-ordinates 
 */

public class EntityController {
	
	private static BaseEntity player;
	private static BaseEntity raindrop;
	private static BaseEntity gameFloor;
	private static ArrayList<BaseEntity> entities = new ArrayList<BaseEntity>();
	private static final String PLAYER_IMAGE = "resources/player1.jpg";
	private static final String GAME_FLOOR_IMAGE = "resources/game_floor.png";
	private static final String RAINDROP_IMAGE = "resources/raindrop.png";
	

	private static int x;
	private static int y;
	
	public static ArrayList<BaseEntity> initialiseEntities(GameEngine game) {

		player = initialisePlayer(game);
		gameFloor = initialiseGameFloor(game);
		
		entities.add(player);
		entities.add(gameFloor);
		
		game.setPlayer(player);
		game.setGameFloor(gameFloor);	

		//create the raindrops
		for (int r = 0; r < StaticVariableController.RAINDROP_COUNT; r++) {
			raindrop = initialiseRaindrop(game);
			game.setRaindrop(raindrop);
			entities.add(raindrop);
		//	raindropCount++;
		}
		return entities;
	}
	
	private static BaseEntity initialiseGameFloor(GameEngine game) {
		x = 0; 
		y = 590;
		return new GameFloor(game, GAME_FLOOR_IMAGE, x, y);
	}
	
	private static BaseEntity initialisePlayer(GameEngine game) {
		x = StaticVariableController.WINDOW_WIDTH/2;
		y = 550;
		// create the first player and place it roughly in the centre of the screen
		return new Player(game,PLAYER_IMAGE, x, y); 
	}

	//this method is also used to create a new raindrop when one hits the floor
	public static BaseEntity initialiseRaindrop(GameEngine game) {
		x = new Random().nextInt(StaticVariableController.WINDOW_WIDTH - 10); //leave a small buffer 
		y = new Random().nextInt(StaticVariableController.WINDOW_HEIGHT/4); //first 1/4 of the screen

		return new Raindrop(game,RAINDROP_IMAGE, x, y);
	}

}
