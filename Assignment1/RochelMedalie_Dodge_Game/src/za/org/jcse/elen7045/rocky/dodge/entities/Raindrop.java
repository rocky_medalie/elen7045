package za.org.jcse.elen7045.rocky.dodge.entities;

import za.org.jcse.elen7045.rocky.dodge.controllers.StaticVariableController;
import za.org.jcse.elen7045.rocky.dodge.game.GameEngine;

/*
 * this class represents the raindrop. Here we check for collisions with various other entities and do something about it.
 * if the raindrop has collided with the player, the game is over
 * if it collided with the game floor, a new raindrop must appear
 */

public class Raindrop extends BaseEntity {
	
	private double moveSpeed = StaticVariableController.RAINDROP_MOVE_SPEED;
	private GameEngine game;

	public Raindrop(GameEngine game,String ref,int x,int y) {
		super(ref,x,y);
		
		this.game = game;
		dy = +moveSpeed;
	}

	public void move(long delta) {
				
		// proceed with normal move
		super.move(delta);
	}
	
	public void handleCollision(BaseEntity other) {
		// if its the game floor that I have collided with, notify the game to remove the raindrop and create a new one
		if (other instanceof GameFloor) {
			game.removeEntity(this);
			game.recycleRaindrop();
		}

		// if its a player that I have collided with, notify the game that the player has lost it's life
		if (other instanceof Player) {
			game.alertDeath();
		}  
	}
}
