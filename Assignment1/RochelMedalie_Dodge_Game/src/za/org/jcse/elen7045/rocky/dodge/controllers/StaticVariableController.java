package za.org.jcse.elen7045.rocky.dodge.controllers;

import java.awt.event.KeyEvent;

public class StaticVariableController {

	//define all static variables that are used throughout the game
	public static final int  RIGHT_BORDER	= 750;
	public static final int  LEFT_BORDER	= 10; 
	public static final int RAINDROP_COUNT = 5;
	public static final int PLAYER_COUNT = 1;
	public static final int WINDOW_WIDTH = 800; //x
	public static final int WINDOW_HEIGHT = 600; //y
	public static double PLAYER_MOVE_SPEED = 1000;
	public static double RAINDROP_MOVE_SPEED = 100 ;
	public static String DodgeGameWindowTitle = "Dodge the Raindrops" ;
	public static String alertInstructions = "Move the player left and right with the arrow keys to dodge the raindrops. Press ESC to end.";
	public static final int LEFT_KEY = KeyEvent.VK_LEFT;
	public static final int RIGHT_KEY = KeyEvent.VK_RIGHT;
	
}
