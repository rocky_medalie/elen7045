package za.org.jcse.elen7045.rocky.dodge.entities;

import za.org.jcse.elen7045.rocky.dodge.game.GameEngine;

public class GameFloor extends BaseEntity {

	/*
	 * this class represents the bottom of the game screen. It does not move or have any logic.
	 * it is used by the raindrop class to detect collision with it
	 */
	private GameEngine game;
	
	public GameFloor(GameEngine game,String ref,int x,int y) {
		super(ref,x,y);  //x location and y location 
		this.game = game;
	}
	
	//don't move
	public void move(long delta) {
		return;
	}
	
	public void handleCollision(BaseEntity other) {
		return;
	}

}
