package za.org.jcse.elen7045.rocky.dodge.controllers;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import javax.imageio.ImageIO;

import za.org.jcse.elen7045.rocky.dodge.entities.Sprite;

/*
 * This class is used to cache the sprite for reference to the sprite instance. 
 * If it's already in memory then no need to get it again
 */

public class SpriteController {

	private static SpriteController singleton = new SpriteController();

	public static SpriteController get() {
		return singleton;
	}
	
	//The cached sprite map, holds reference to the sprite instance
	private HashMap<String, Sprite> sprites = new HashMap<String, Sprite>();
	
	public Sprite getSprite(String spriteRef) {
		// if we've already got the sprite in the cache then just return the existing version
		if (sprites.get(spriteRef) != null) {
			return (Sprite) sprites.get(spriteRef);
		}
		
		// otherwise, get the sprite from the resource loader
		BufferedImage spriteImage = null;
		
		try {
			URL url = this.getClass().getClassLoader().getResource(spriteRef);
			
			if (url == null) {
				fail("sprite reference not found: " + spriteRef);
			}
			
			// use ImageIO to read the image
			spriteImage = ImageIO.read(url);
			
		} catch (IOException e) {
			fail("Failed to load ref: " + spriteRef);
		}
		
		// create an accelerated image of the right size to store our sprite in
		GraphicsConfiguration raphicsConfig = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
		Image image = raphicsConfig.createCompatibleImage(spriteImage.getWidth(),spriteImage.getHeight(),Transparency.BITMASK);
		
		// draw the source image into the accelerated image
		image.getGraphics().drawImage(spriteImage,0,0,null);
		
		// create a sprite, add it to the cache then return it
		Sprite sprite = new Sprite(image);
		sprites.put(spriteRef,sprite);
		
		return sprite;
	}

	private void fail(String message) {
		System.err.println(message);
		System.exit(0); //cant continue if there is no image
	}
}