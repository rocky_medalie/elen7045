package za.org.jcse.elen7045.rocky.dodge.game;


import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import za.org.jcse.elen7045.rocky.dodge.controllers.EntityController;
import za.org.jcse.elen7045.rocky.dodge.controllers.KeyboardInputController;
import za.org.jcse.elen7045.rocky.dodge.controllers.StaticVariableController;
import za.org.jcse.elen7045.rocky.dodge.entities.BaseEntity;


public class GameEngine extends Canvas {

	/*
	 * this is the actual game logic. 
	 * here we create a window where the game will exist in, and a loop to keep rendering the game
	 */

	private static final long serialVersionUID = 1L;
	private BaseEntity player;
	private BaseEntity raindrop;
	private BaseEntity gameFloor;
	private List<BaseEntity> entities = new ArrayList<BaseEntity>();
	private List<BaseEntity> removeList = new ArrayList<BaseEntity>();
	private boolean gameRunning = true;
	private BufferStrategy strategy;
	private boolean waitingToStart = true;
	private String alertMessage = "";
	private String alertInstructions = StaticVariableController.alertInstructions;
	private boolean leftArrowPressed = false;
	private boolean rightArrowPressed = false;
	private int windowWidth = StaticVariableController.WINDOW_WIDTH;
	private int windowHeight = StaticVariableController.WINDOW_HEIGHT;
	private long loopTime;
	private Graphics2D graphics2D;

	public GameEngine() {

		JFrame window = new JFrame();
		window.setBounds(450, 50, windowWidth, windowHeight);
		window.setTitle(StaticVariableController.DodgeGameWindowTitle);

		JPanel panel = (JPanel) window.getContentPane();
		panel.setPreferredSize(new Dimension(windowWidth, windowHeight));
		panel.setLayout(null);

		// setup our canvas size and put it into the content of the frame
		setBounds(0, 0, windowWidth, windowHeight);
		panel.add(this);

		//setIgnoreRepaint(true);
		//Causes this Window to be sized to fit the preferred size
		window.pack();
		window.setResizable(false);
		window.setVisible(true);

		//listen for key inputs
		addKeyListener(new KeyboardInputController(this)); 
		//so we can get alert of key input
		requestFocus();  

		//create the buffering strategy which will allow AWT to manage our accelerated graphics
		//Multi-buffering is useful for rendering performance
		createBufferStrategy(2);
		strategy = getBufferStrategy();

		// initialise the entities in our game so there's something to see at startup
		entities = EntityController.initialiseEntities(this);
	}

	// Start the game. Clear all variables
	public void startGame() {
		// clear out any existing entities and intialise a new set
		entities.clear();
		entities = EntityController.initialiseEntities(this);

		// blank out any keyboard settings we might currently have
		leftArrowPressed = false;
		rightArrowPressed = false;
	}

	//when a raindrop hits the ground remove it from the entity list
	public void removeEntity(BaseEntity entity) {
		removeList.add(entity);
	}

	//notification that the game is over
	public void alertDeath() {
		alertMessage = "Game Over!";
		waitingToStart = true;
	}

	public void recycleRaindrop() {
		// add another raindrop
		raindrop = EntityController.initialiseRaindrop(this);
		entities.add(raindrop);
	}
	
	
	/* brute force collisions, compare every entity against
	 every other entity. If any of them collide notify 
	 both entities that the collision has occured*/
	private void bruteForceCollisionCheck() {
		for (int m = 0; m < entities.size(); m++) {
			for (int o = m + 1; o < entities.size(); o++) {
				BaseEntity me = (BaseEntity) entities.get(m);
				BaseEntity other = (BaseEntity) entities.get(o);
				//check to see if we have a collision
				if (me.collidesWith(other)) {
					//call the method on the entity to determine what to do about the collision
					me.handleCollision(other);
					other.handleCollision(me);
				}
			}
		}
	}
	
	private void drawAndMoveEntities() {
		for (int i=0; i<entities.size(); i++) {
			BaseEntity entity = (BaseEntity) entities.get(i);
			entity.draw(graphics2D); //draw each entity
			if (!waitingToStart) {
				entity.move(loopTime);  //move each entity if game is in play
			}
		}
	}
	
	private void cleanUpEntities() {
		// remove any entity that has been marked for clear up
		entities.removeAll(removeList);
		removeList.clear();
	}
	
	private void showMessage() {
		// if we're waiting for an "any key" press to start the game then draw the current message 
		// if the game is over then draw the game over message
		if (waitingToStart) {
			graphics2D.setColor(Color.black);
			graphics2D.drawString(alertMessage,(windowWidth-graphics2D.getFontMetrics().stringWidth(alertMessage))/2,250);
			if (alertMessage == "") {
				graphics2D.drawString(alertInstructions,(windowWidth-graphics2D.getFontMetrics().stringWidth(alertInstructions))/2,270);
				}
			graphics2D.drawString("Press any key to start",(windowWidth-graphics2D.getFontMetrics().stringWidth("Press any key to play"))/2,310);
			}	
	}
	
	private void setPlayerSpeed() {
		//at first the player doesn't move. If a key is pressed, move the player accordingly
		player.setHorizontalSpeed(0);

		if ((leftArrowPressed) && (!rightArrowPressed)) {
			player.setHorizontalSpeed(-player.getMoveSpeed());
		} else if ((rightArrowPressed) && (!leftArrowPressed)) {
			player.setHorizontalSpeed(player.getMoveSpeed());
		}
	}
	
	private void sleep() {
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	//This is the game loop
	public void play() {
		long lastLoopTime = System.currentTimeMillis();
		// keep looping round till the game ends
		while (gameRunning) {
			// work out how long its been since the last update, this will be used to calculate how far the entities should move this loop
			loopTime = System.currentTimeMillis() - lastLoopTime;
			lastLoopTime = System.currentTimeMillis();

			// Get hold of a graphics context for the accelerated surface and blank it out
			graphics2D = (Graphics2D) strategy.getDrawGraphics();
			graphics2D.setColor(Color.white);
			graphics2D.fillRect(0, 0, windowWidth, windowHeight);
			
			showMessage();
			
			setPlayerSpeed();
			
			// loop through all entities we ahve in the game, drawing all and if applicable, move them
			drawAndMoveEntities();
		
			bruteForceCollisionCheck();
			
			cleanUpEntities();
			
			graphics2D.dispose();
			strategy.show();

			// pause for a bit. Note: this should run us at about 100 fps 
			sleep();
		}
	}
	
	//getters and setters
	public BaseEntity getPlayer() {
		return player;
	}

	public void setPlayer(BaseEntity player) {
		this.player = player;
	}

	public BaseEntity getRaindrop() {
		return raindrop;
	}

	public void setRaindrop(BaseEntity raindrop) {
		this.raindrop = raindrop;
	}

	public BaseEntity getGameFloor() {
		return gameFloor;
	}

	public void setGameFloor(BaseEntity gameFloor) {
		this.gameFloor = gameFloor;
	}

	public ArrayList<BaseEntity> getEntities() {
		return (ArrayList<BaseEntity>) entities;
	}

	public void setEntities(ArrayList<BaseEntity> entities) {
		this.entities = entities;
	}

	public boolean isLeftArrowPressed() {
		return leftArrowPressed;
	}

	public void setLeftArrowPressed(boolean leftArrowPressed) {
		this.leftArrowPressed = leftArrowPressed;
	}

	public boolean isRightArrowPressed() {
		return rightArrowPressed;
	}

	public void setRightArrowPressed(boolean rightArrowPressed) {
		this.rightArrowPressed = rightArrowPressed;
	}
	
	public boolean isWaitingToStart() {
		return waitingToStart;
	}

	public void setWaitingToStart(boolean waitingToStart) {
		this.waitingToStart = waitingToStart;
	}
}

