package za.org.jcse.elen7045.rocky.dodge.entities;

import java.awt.Graphics;
import java.awt.Image;


public class Sprite {
	/*
	 * this class is just the sprite that will be displayed on the screen. Its the image and the location on the screen 
	 * we can use a sprite in lots of different places without storing lots of copies of the image
	 */
	
	private Image image;
	
	public Sprite(Image image) {
		this.image = image;
	}
	
	public int getWidth() {
		return image.getWidth(null);
	}

	public int getHeight() {
		return image.getHeight(null);
	}

	//draw the entity to the graphics context. every entity will call this method to draw itself
	//it uses java.awt.Graphics class to draw the image. There is no image observer
	public void draw(Graphics graphics, int x, int y) {
		graphics.drawImage(image, x, y, null);
	}
}
